from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('spell', views.spell, name='spell')
]