from django.shortcuts import render, redirect
from django.http import HttpResponse

import pyttsx3

# Create your views here.
def home(request):  
    return render(request,'index.html')

def spell(request):
    word = request.POST["word"]

    for letter in word:
        engine = pyttsx3.init()
        engine.say(letter)
        engine.runAndWait()
        rate = engine.getProperty('rate')  
        engine.setProperty('rate', 100)    
        volume = engine.getProperty('volume')   
        engine.setProperty('volume',1.0)    
        voices = engine.getProperty('voices')       
        engine.setProperty('voice', voices[0].id)   

    engine = pyttsx3.init()
    engine.say(word)
    engine.runAndWait()
    rate = engine.getProperty('rate')  
    engine.setProperty('rate', 120)    
    volume = engine.getProperty('volume')   
    engine.setProperty('volume',1.0)    
    voices = engine.getProperty('voices')       
    engine.setProperty('voice', voices[0].id) 

    return render(request,'index.html')
