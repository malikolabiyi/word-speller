from django.apps import AppConfig


class WordspellerAppConfig(AppConfig):
    name = 'wordspeller_app'
